package com.example.AngularSpringBoot.Service;

import java.util.List;
import java.util.Map;

import com.example.AngularSpringBoot.Model.EmployeeEntity;

public interface EmployeeService {
	public Map<String,Object> saveEmployeeDetails(EmployeeEntity employee);
	public Map<String,Object> getEmpById(Long id);
	public List<Map<String,Object>> getAllEmp();
	public int updateEmployeeById(Map<String,Object> map);
	public int deleteEmployeeById(Long id);

}
