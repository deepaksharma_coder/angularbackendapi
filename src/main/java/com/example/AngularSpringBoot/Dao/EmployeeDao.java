package com.example.AngularSpringBoot.Dao;

import java.util.List;
import java.util.Map;

import com.example.AngularSpringBoot.Model.EmployeeEntity;

public interface EmployeeDao {
	public void saveEmployeeDetails(EmployeeEntity employee);
	public Map<String,Object> getEmpById(Long id);
	public List<Map<String,Object>> getEmps();
	public int updateEmployeeDetailsById(Map<String,Object> map);
	public int deleteEmployeeDetailsById(Long id);

}
