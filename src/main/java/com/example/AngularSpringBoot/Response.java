package com.example.AngularSpringBoot;

public class Response {
	private String message;
	private String developerMessage;
	private String status;
	private Object obj;
	
	public Response(String message, String developerMessage, String status,Object obj) {
		this.message = message;
		this.developerMessage = developerMessage;
		this.status = status;
		this.obj=obj;
	}

}
