package com.example.AngularSpringBoot.ServiceImpl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.AngularSpringBoot.Dao.EmployeeDao;
import com.example.AngularSpringBoot.Model.EmployeeEntity;
import com.example.AngularSpringBoot.Service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public Map<String, Object> saveEmployeeDetails(EmployeeEntity employee) {
		// TODO Auto-generated method stub
		//EmployeeEntity employee=new ObjectMapper().convertValue(map, EmployeeEntity.class);
		employeeDao.saveEmployeeDetails(employee);
		return employeeDao.getEmpById(employee.getId());
	}

	@Override
	public Map<String, Object> getEmpById(Long id) {
		// TODO Auto-generated method stub
		return employeeDao.getEmpById(id);
	}

	@Override
	public int updateEmployeeById(Map<String, Object> map) {
		// TODO Auto-generated method stub
		int row=employeeDao.updateEmployeeDetailsById(map);
		return row;
	}

	@Override
	public int deleteEmployeeById(Long id) {
		// TODO Auto-generated method stub
		return employeeDao.deleteEmployeeDetailsById(id);
			
	}

	@Override
	public List<Map<String, Object>> getAllEmp() {
		// TODO Auto-generated method stub
		return employeeDao.getEmps();
	}

}
