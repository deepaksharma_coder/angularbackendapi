package com.example.AngularSpringBoot.DaoImpl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;

public class AbstractDao<Pk extends Serializable,T> {
	private Class<T> persistenceClass;
	@SuppressWarnings("unchecked")
	public AbstractDao(){
		this.persistenceClass=(Class<T>) ((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];

	}
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public void save(T entity){
		Session session=(Session) entityManager.unwrap(org.hibernate.Session.class);
		session.save(entity);
		session.flush();
		
	}
	
	public Query createQuery(String hql){
		Session session=(Session) entityManager.unwrap(org.hibernate.Session.class);
		return session.createQuery(hql);
	}
	
	public Criteria createCriteria(){
		Session session=(Session) entityManager.unwrap(org.hibernate.Session.class);
		return session.createCriteria(persistenceClass);
		
	}

}
