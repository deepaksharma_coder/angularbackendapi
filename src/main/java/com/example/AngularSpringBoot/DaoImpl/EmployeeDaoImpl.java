package com.example.AngularSpringBoot.DaoImpl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.example.AngularSpringBoot.Dao.EmployeeDao;
import com.example.AngularSpringBoot.Model.EmployeeEntity;

@Repository("employeeDaoImpl")
public class EmployeeDaoImpl extends AbstractDao<Long, EmployeeEntity> implements EmployeeDao{
	
	private ProjectionList employeeDetailProjection(){
		return Projections.projectionList()
				.add(Projections.property("id"),"id")
				.add(Projections.property("firstName"),"firstName")
				.add(Projections.property("middleName"),"middleName")
				.add(Projections.property("lastName"),"lastName")
				.add(Projections.property("dob"),"dob")
				.add(Projections.property("emailId"),"emailId")
				.add(Projections.property("houseNo"),"houseNo")
				.add(Projections.property("streetNo"),"streetNo")
				.add(Projections.property("pincode"),"pincode")
				.add(Projections.property("state"),"state")
				.add(Projections.property("country"),"country");
	} 

	@Override
	public void saveEmployeeDetails(EmployeeEntity employee) {
		// TODO Auto-generated method stub
		save(employee);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getEmpById(Long id) {
		// TODO Auto-generated method stub
		return (Map<String,Object>) createCriteria()
				.add(Restrictions.eq("id", id))
				.setProjection(employeeDetailProjection())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
	}
	
	@Override
	public List<Map<String, Object>> getEmps() {
		// TODO Auto-generated method stub
		return (List<Map<String,Object>>) createCriteria()
				.setProjection(employeeDetailProjection())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.list();
	}

	@Override
	public int updateEmployeeDetailsById(Map<String, Object> map) {
		// TODO Auto-generated method stub
		String hql="update EmployeeEntity set firstName=:first,"
				+ "middleName=:middle,"
				+ "lastName=:last,"
				+ "dob=:dob,"
				+ "emailId=:emailId,"
				+ "houseNo=:houseNo,"
				+ "streetNo=:streetNo,"
				+ "pincode=:pincode,"
				+ "state=:state,"
				+ "country=:country where id=:id";
		Query query=createQuery(hql);
		query.setParameter("first", map.get("firstName").toString());
		query.setParameter("middle", map.get("middleName").toString());
		query.setParameter("last", map.get("lastName").toString());
		query.setParameter("dob", map.get("dob").toString());
		query.setParameter("emailId", map.get("emailId").toString());
		query.setParameter("houseNo", map.get("houseNo").toString());
		query.setParameter("streetNo", map.get("streetNo").toString());
		query.setParameter("pincode", map.get("pincode").toString());
		query.setParameter("state", map.get("state").toString());
		query.setParameter("country", map.get("country").toString());
		query.setParameter("id", Long.parseLong(map.get("id").toString()));
		return query.executeUpdate();
		
	}

	@Override
	public int deleteEmployeeDetailsById(Long id) {
		// TODO Auto-generated method stub
		String hql="Delete from EmployeeEntity where id=:id";
		Query query=createQuery(hql);
		query.setParameter("id", id);
		return query.executeUpdate();
	}

}
