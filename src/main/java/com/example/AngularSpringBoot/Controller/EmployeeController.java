package com.example.AngularSpringBoot.Controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.AngularSpringBoot.Model.EmployeeEntity;
import com.example.AngularSpringBoot.Service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@Value("${report.header}")
	private String header;
	
	//save employee Deatils...
	
	@CrossOrigin
	@RequestMapping(value="/saveEmpDetails",method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<String> saveEmpDetails(@Valid @RequestBody EmployeeEntity employee,BindingResult bindingResult) throws JsonProcessingException{
		try{
			String message="";
			if(bindingResult.hasErrors()){
				System.out.println("csacscsa");
				List<FieldError> errors = bindingResult.getFieldErrors();
				for(FieldError e:errors){
					message+=e.getField()+":"+e.getDefaultMessage();
				}
				return new ResponseEntity<>(new ObjectMapper().writeValueAsString(message),HttpStatus.BAD_REQUEST);
			}else{
				Map<String,Object> persist=employeeService.saveEmployeeDetails(employee);
				message="Successfully saved";
				return new ResponseEntity<>(new ObjectMapper().writeValueAsString(message),HttpStatus.CREATED);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Internal server error..."),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//read employee by EmployeeId...

	@CrossOrigin
	@RequestMapping(value="/getDetails/{id}",method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getEmpDetailById(@PathVariable("id") Long id) throws JsonProcessingException{
		try{
			Map<String,Object> employee=employeeService.getEmpById(id);
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString(employee),HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Internal server error..."),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//read All employees...

	@CrossOrigin
	@RequestMapping(value="/getAllEmployees",method=RequestMethod.GET)
	public ResponseEntity<String> getAllEmployees() throws JsonProcessingException{
		try{
			List<Map<String,Object>> employee=employeeService.getAllEmp();
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString(employee),HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Internal server error..."),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//update employee Deatils....
	
	@CrossOrigin
	@RequestMapping(value="/updateEmpDetails/{id}",method=RequestMethod.PUT,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<String> updateEmpDetailById(@PathVariable("id") Long id,@RequestBody Map<String,Object> map) throws JsonProcessingException{
		map.put("id", id);
		try{
			if(employeeService.updateEmployeeById(map) > 0){
				return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Update sucessfully..."),HttpStatus.CREATED);
			}else{
				return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Something went wrong..."),HttpStatus.CREATED);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Internal server error..."),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//delete employee Deatils...
	
	@CrossOrigin
	@RequestMapping(value="/deleteEmpDeatil/{id}",method=RequestMethod.DELETE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<String> deleteEmpById(@PathVariable("id") Long id) throws JsonProcessingException{
		try{
			int row=employeeService.deleteEmployeeById(id);
			if(row>0)
			    return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Delete successfully"),HttpStatus.CREATED);
			else
				return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Something went wrong"),HttpStatus.CREATED);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Internal server error..."),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//First Method using Resource
	@CrossOrigin
	@RequestMapping(value="/downloadReportOfAllEmployees",method=RequestMethod.GET)
	private ResponseEntity<Resource> downloadReport() throws IOException{
		List<Map<String,Object>> employee=employeeService.getAllEmp();
		String str=header+"\n";
		int i=0;
		for(Map<String,Object> emp:employee){
			str+=++i + ","+emp.get("id").toString()
					+","+emp.get("firstName").toString()
					+","+emp.get("middleName").toString()
					+","+emp.get("lastName").toString()
					+","+emp.get("dob").toString()
					+","+emp.get("emailId").toString()
					+","+emp.get("houseNo").toString()
					+","+emp.get("streetNo").toString()
					+","+emp.get("pincode").toString()
					+","+emp.get("state").toString()
					+","+emp.get("country").toString()+"\n";
			
		}
		Path path;
		//System.out.println(str);
		Path root=Paths.get(".");
		path=Paths.get("D:/Chrome-Download/AngularApi/src/main/resources/","report");
		if(Files.exists(path)){
			System.out.println("exits...");
			path=Paths.get(path.toString()+"FirstReport.txt");
		}else{
			path=Files.createDirectories(path);
			path=Files.createFile(Paths.get(path.toString(),"FirstReport.txt"));
			
		}
		Files.write(path, str.getBytes(), StandardOpenOption.CREATE);
		HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=demo.csv");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
		return ResponseEntity.ok()
				.headers(header)
                .contentLength(Files.readAllBytes(path).length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body( new ByteArrayResource(Files.readAllBytes(path)));
		//return new ResponseEntity<byte[]>(Files.readAllBytes(path), header, HttpStatus.OK);
		//System.out.println(path);
		//return new ResponseEntity<>(new ObjectMapper().writeValueAsString(str),HttpStatus.OK);
		
	}
	
	//Second Method
	
	@CrossOrigin
	@RequestMapping(value="/report",method=RequestMethod.GET)
	private ResponseEntity<String> downloadReportUsingCsv() throws IOException{
		try {
			List<Map<String,Object>> employee=employeeService.getAllEmp();
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString(employee),HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ObjectMapper().writeValueAsString("Internal Server Error...."),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
